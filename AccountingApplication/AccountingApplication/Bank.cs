﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    public partial class Bank : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        internal Decimal newBal; //CB set as internal so can be accessed from all forms in the application
        internal Decimal currBal;
        internal Decimal amount;
        internal bool rtnvalue;
        public Bank()
        {
            InitializeComponent();
            mySqlConn.Open();
        }
        public decimal Balance //CB Decimal set as method as to allow for queries to be run
        {
            get //CB get method used to retrieve balance
            {
                if (!string.IsNullOrEmpty(lblCustId.Text)) //CB makes sure customer id can be found to use in query
                {
                    SqlCeCommand scc = new SqlCeCommand("Select * From Account where  Cust_ID = '" + lblCustId.Text + "'", mySqlConn);
                    using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
                    {
                        DataTable dt = new DataTable();
                        dt.Load(mySqlDR);
                        try
                        {
                            return currBal = (decimal)dt.Rows[0]["Balance"];
                        }
                        catch (IndexOutOfRangeException)
                        {
                            MessageBox.Show("Error");
                            return currBal;
                        }
                    }
                }
                else
                {                    
                    return currBal;
                }
            }
        }
        public decimal setBal //CB Sets balance as passed value
        {
            set { currBal = value; }
        }
        public decimal setNewBal//CB Sets updated balance as passed value
        {
            set { newBal = value; }
        }
        public decimal newBalance //CB Gets new balance
        {
            get { return newBal; }
        }
        public decimal setDebit //CB Sets debit amount
        {
            set { amount = value; }
        }
        public decimal getDebit //CB gets debit amount
        {
            get { return amount; }
        }
        public decimal setCredit //CB Sets credit amount
        {
            set { amount = value; }
        }
        public decimal getCredit //CB gets credit amount
        {
            get { return amount; }
        }

        public bool setRtnValue //CB sets returned bool value
        {
            set { rtnvalue = value; }
        }
        public bool getRtnValue //CB gets returned bool value
        {
            get { return rtnvalue; }
        }

        public bool creditAccount() //CB Used to credit account
        {
            setRtnValue = true; //CB uses set method from above to set bool
            decimal? nullAmount = amount; //CB sets amount retrieved from input box as a nullable decimal
            //CB Checks whether the credit submit box is null or if account balance has value
            //CB Nullable decimal use to stop error being thrown if no amount input
            if (!string.IsNullOrEmpty(txtCredit.Text) || nullAmount.HasValue)
            {
                if (!string.IsNullOrEmpty(txtCredit.Text))//CB checks to make sure input isnt null and updates balance
                {
                    currBal = Balance; //CB sets currBal using Balance method
                    setCredit = Decimal.Parse(txtCredit.Text.ToString()); //CB converts textbox into decimal format to allow for insertion into database 
                    amount = getCredit; //Cb return credit and sets it as amount
                }

                if (amount <= 0) //CB checks to see if amount entered is less than 0 and returns error if true
                {                    
                    MessageBox.Show("Amount To Credit Must Be Greater Than 0.");
                    txtCredit.Text = txtDebit.Text = "";
                    //rtnvalue = false;
                    setRtnValue = false;
                    //throw new ArgumentOutOfRangeException("amount");
                }
                else //CB if all above are passed then the balance is updated and will update balance in database when button clicked.
                {
                    setNewBal = currBal + amount;
                    //rtnvalue = true;
                    setRtnValue = true;
                }
            }
            else
            {
                MessageBox.Show("Please Enter a Value."); //CB if textbox empty error returned
                //rtnvalue = false;
                setRtnValue = false;
            }
            amount = 0;
            return (getRtnValue);
        }

        public bool debitAccount() //CB Used to debit account
        {
            setRtnValue = true; //CB uses set method from above to set bool             
            decimal? nullAmount = amount; //CB sets amount retrieved from input box as a nullable decimal
            //CB Checks whether the debit submit box is null or if account balance has value
            //CB Nullable decimal use to stop error being thrown if no amount input
            if (!string.IsNullOrEmpty(txtDebit.Text) || nullAmount.HasValue)
            {
                if (!string.IsNullOrEmpty(txtDebit.Text))//CB checks to make sure input isnt null and updates balance
                {
                    currBal = Balance; //CB sets currBal using Balance method   
                    setDebit = Decimal.Parse(txtDebit.Text.ToString()); //CB converts textbox into decimal format to allow for insertion into database
                    amount = getDebit; //Cb return debit and sets it as amount
                }

                if (amount <= 0)//CB checks to see if amount entered is less than 0 and returns error if true
                {
                    MessageBox.Show("Amount To Debit Must Be Greater Than 0.");
                    txtCredit.Text = txtDebit.Text = "";
                    rtnvalue = false;
                    setRtnValue = false;
                    //throw new ArgumentOutOfRangeException("amount");
                }
                else if (amount > currBal) //CB makes sure debit amount isnt greater than balance. returns error if true
                {
                    MessageBox.Show("You Do Not Have Enough In Your Account To Debit This Amount.");
                    txtCredit.Text = txtDebit.Text = "";
                    //rtnvalue = false;
                    setRtnValue = false;
                    //throw new ArgumentOutOfRangeException("amount");
                }
                else
                {
                    setNewBal = currBal - amount;
                    //rtnvalue = true;
                    setRtnValue = true;
                }
            }
            else //CB if all above are passed then the balance is updated and will update balance in database when button clicked.
            {
                MessageBox.Show("Please Enter a Value.");
                //rtnvalue = false;
                setRtnValue = true;
            }
            amount = 0;                     
            return (getRtnValue);
        }

        public void updateBalance() //CB Method used to update. only called if all queries are passed in credit/debit account methods
        {
            try
            {
                SqlCeCommand cmd = new SqlCeCommand("UPDATE Account SET Balance = @balance  WHERE Cust_ID = @custId", mySqlConn);
                cmd.Parameters.AddWithValue("@balance", decimal.Round(newBalance,2));
                cmd.Parameters.AddWithValue("@custId", lblCustId.Text);
                cmd.ExecuteNonQuery();
                lblBal.Text = newBalance.ToString("0.00");
                this.Refresh();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(lblAccount.Text + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void cleartxtBoxes()
        {
            txtCredit.Text = txtDebit.Text = "";
        }

        private void button1_Click(object sender, EventArgs e) //CB this button is used to credit account
        {
            if (creditAccount())
            {
                updateBalance();
                cleartxtBoxes();
                MessageBox.Show("Account Credited");
            }
        }

        private void button2_Click(object sender, EventArgs e) //CB this button is used to debit account
        {
            if (debitAccount())
            {
                updateBalance();
                cleartxtBoxes();
                MessageBox.Show("Account Debited");
            }
        }
    }
}
