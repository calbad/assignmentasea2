﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    public partial class AdminEditUser : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        String adminChecked = "0";
        public AdminEditUser()
        {
            InitializeComponent();
            mySqlConn.Open();
        }

        public void userSearch()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Users where  Username = '" + txtUser.Text + "'", mySqlConn);

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    label7.Text = dt.Rows[0]["Username"].ToString();
                    label8.Text = dt.Rows[0]["Email"].ToString();
                    adminChecked = dt.Rows[0]["Admin"].ToString();

                    if (adminChecked == "1")
                    {
                        checkBox1.Checked = true;
                    }
                    else if (adminChecked == "0")
                    {
                        checkBox1.Checked = false;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("We Don't Have Any Information Related to This User");
                }
            }
        }

        public void idSearch()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Users where  ID = '" + txtId.Text + "'", mySqlConn);          

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    label7.Text = dt.Rows[0]["Username"].ToString();
                    label8.Text = dt.Rows[0]["Email"].ToString();
                    adminChecked = dt.Rows[0]["Admin"].ToString();

                    if (adminChecked == "1")
                    {
                        checkBox1.Checked = true;
                    }
                    else if (adminChecked == "0")
                    {
                        checkBox1.Checked = false;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("We Don't Have Any Information Related to This User");
                }
            }
        }

        public void cleartxtBoxes()
        {
            txtUser.Text = txtId.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            userSearch();
            cleartxtBoxes();
            this.Refresh();
        }

        public void updateRecord()
        {
            try
            {
                SqlCeCommand cmd = new SqlCeCommand("UPDATE Users set Admin = @admin WHERE Username = '" + label7.Text + "'", mySqlConn);
                if (checkBox1.Checked)
                {
                    adminChecked = "1";
                }
                else if (!checkBox1.Checked)
                {
                    adminChecked = "0";
                }
                cmd.Parameters.AddWithValue("@admin", adminChecked);
                cmd.ExecuteNonQuery();
            }
            catch (SqlCeException)
            {
                MessageBox.Show("No User Details Loaded.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            idSearch();
            cleartxtBoxes();
            this.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            updateRecord();
            MessageBox.Show("Privileges Updated");
            this.Refresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin admin = new Admin();
            admin.Show();
        }
    }
}
