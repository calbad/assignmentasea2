﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    //CB This page is used by admin to create a bank account
    public partial class AdminAddAccount : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        public AdminAddAccount()
        {
            InitializeComponent();
            mySqlConn.Open();
        }

        public bool checkInputs() //CB Checks if inputs are null
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtNumber.Text) ||
                string.IsNullOrEmpty(txtSort.Text) ||
                string.IsNullOrEmpty(txtType.Text) ||
                string.IsNullOrEmpty(txtBalance.Text) ||
                string.IsNullOrEmpty(txtId.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public bool checkNumber() //CB Used to check whether Account number is already in use
        {
            SqlCeCommand scc = new SqlCeCommand("Select Count(*) From Account where  [Account Number] = '" + txtNumber.Text + "'", mySqlConn);

            bool rtnvalue = true;
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                    if (dt.Rows[0][0].ToString() == "1")
                    {
                        MessageBox.Show("This Account Number is already taken, please choose another");
                        rtnvalue = false;
                    }

                    return (rtnvalue);                
            }
        }

        public bool checkId()//CB Checks whether user ID input relates to a user who already has an account
        {
            SqlCeCommand scc = new SqlCeCommand("Select Count(*) From Account where  Cust_ID = '" + txtId.Text + "'", mySqlConn);

            bool rtnvalue = true;
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);                
                    if (dt.Rows[0][0].ToString() == "1")
                    {
                        MessageBox.Show("This Customer already has an account");
                        rtnvalue = false;
                    }

                    return (rtnvalue);                
            }
        }

        public void cleartxtBoxes()//CB Clear all text boxes once account is added
        {
            txtNumber.Text = txtSort.Text = txtType.Text = txtBalance.Text = txtId.Text = "";
        }

        public void insertRecord() //CB Used to insert bank account into database
        {
            try
            {                
                SqlCeCommand cmd = new SqlCeCommand("INSERT INTO Account([Account Number], [Sort Code], [Account Type], Balance, Cust_ID) VALUES (@number, @sort, @type, @balance, @cust)", mySqlConn);
                cmd.Parameters.AddWithValue("@number", txtNumber.Text);
                cmd.Parameters.AddWithValue("@sort", txtSort.Text);
                cmd.Parameters.AddWithValue("@type", txtType.Text);
                cmd.Parameters.AddWithValue("@balance", txtBalance.Text);
                cmd.Parameters.AddWithValue("@cust", txtId.Text);
                cmd.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(txtNumber.Text + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e) //CB Allows admin to go back to admin page if doesnt want to take action
        {
            this.Hide();
            Admin admin = new Admin();
            admin.Show();
        }

        private void button2_Click(object sender, EventArgs e) //CB used to evaluate adn call insertRecord
        {
            if (checkInputs() && checkNumber() && checkId())
            {
                insertRecord();
                cleartxtBoxes();
                MessageBox.Show("Account Created");
                this.Refresh();
            }
        }
    }
}
