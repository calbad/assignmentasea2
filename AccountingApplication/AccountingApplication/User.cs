﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    public partial class User : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        String pass;
        String userId;
        String currentUser;
        String currentEmail;
        String mainText;

        public User()
        {
            InitializeComponent();
            txtUser.Text = Main._textBox1;
            mainText = Main._textBox1;
            mySqlConn.Open();
            extractDetails();
        }
       
        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtUser.Text) ||
                string.IsNullOrEmpty(txtEmail.Text))
            {
                MessageBox.Show("Error: Please check your Username and Email Inputs");
                rtnvalue = false;
            }

            return (rtnvalue);
        }
        
        public bool checkPasswords()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtOld.Text) &&
                string.IsNullOrEmpty(txtNew.Text) &&
                string.IsNullOrEmpty(txtConf.Text))
            {
                rtnvalue = true;
            }
            else if (!string.IsNullOrEmpty(txtOld.Text) &&
                     !string.IsNullOrEmpty(txtNew.Text) &&
                     !string.IsNullOrEmpty(txtConf.Text))
            {
                rtnvalue = true;
            }
            else
            {
                MessageBox.Show("Error with password fields. Please Try Again. All Must be Filled or All Left Empty");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public bool checkNewMatch()
        {
            bool rtnvalue = true;

            if (txtNew.Text != txtConf.Text)
            {
                MessageBox.Show("Error: Passwords do not match. Please Try Again");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public bool checkUser()
        {
            SqlCeCommand scc = new SqlCeCommand("Select Count(*) From Users where  Username = '" + currentUser + "'", mySqlConn);            

            bool rtnvalue = true;
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                SqlCeCommand sccB = new SqlCeCommand("Select * From Users where  Username = '" + currentUser + "'", mySqlConn);
                using (SqlCeDataReader mySqlDRB = sccB.ExecuteReader())
                {
                    DataTable dtB = new DataTable();
                    dtB.Load(mySqlDRB);
                    if (dt.Rows[0][0].ToString() == "1" && dtB.Rows[0]["Username"].ToString() != currentUser)
                    {
                        MessageBox.Show(mainText + " This Username is already taken, please choose another");
                        rtnvalue = false;
                    }

                    return (rtnvalue);
                }
            }
        }

        public bool checkEmail()
        {
            SqlCeCommand scc = new SqlCeCommand("Select Count(*) From Users where  Email = '" + currentEmail + "'", mySqlConn);

            bool rtnvalue = true;
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                SqlCeCommand sccB = new SqlCeCommand("Select * From Users where  Email = '" + currentEmail + "'", mySqlConn);
                using (SqlCeDataReader mySqlDRB = sccB.ExecuteReader())
                {
                    DataTable dtB = new DataTable();
                    dtB.Load(mySqlDRB);
                    if (dt.Rows[0][0].ToString() == "1" && dtB.Rows[0]["Email"].ToString() != currentEmail)
                    {
                        MessageBox.Show(currentEmail + " This Email is already taken, please choose another");
                        rtnvalue = false;
                    }

                    return (rtnvalue);
                }
            }
        }

        public bool checkCurrentPass()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Users where  Username = '" + txtUser.Text + "'", mySqlConn);

            bool rtnvalue = true;
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {

                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                if (!string.IsNullOrEmpty(txtOld.Text))
                {
                    if (dt.Rows[0]["Password"].ToString() != txtOld.Text)
                    {
                        MessageBox.Show("This isn't your current password, please try again");
                        rtnvalue = false;
                    }
                }
                else
                {
                    rtnvalue = true;
                }

                return (rtnvalue);
            }
        }

        public void extractDetails()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Users where  Username = '" + txtUser.Text + "'", mySqlConn);

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);                
                try
                {
                    userId = dt.Rows[0]["ID"].ToString();
                    txtUser.Text = dt.Rows[0]["Username"].ToString();
                    currentUser = txtUser.Text;
                    pass = dt.Rows[0]["Password"].ToString();
                    txtEmail.Text = dt.Rows[0]["Email"].ToString();
                    currentEmail = txtEmail.Text;
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("Error");
                }
            }            
        }

        public void updateRecord()
        {
            try
            {
                SqlCeCommand cmd = new SqlCeCommand("UPDATE Users set Username = @user, Password = @pass, Email = @email WHERE ID = @id", mySqlConn);
                
                cmd.Parameters.AddWithValue("@id", userId);
                cmd.Parameters.AddWithValue("@user", txtUser.Text);
                if(string.IsNullOrEmpty(txtNew.Text))
                {
                    cmd.Parameters.AddWithValue("@pass", pass);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@pass", txtNew.Text);
                }
                cmd.Parameters.AddWithValue("@email", txtEmail.Text);
                
                cmd.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(txtUser.Text + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkInputs() && checkPasswords() && checkNewMatch() && checkUser() && checkEmail() && checkCurrentPass())
            {
                this.Hide();
                updateRecord();
                MessageBox.Show("Details Updated");        
                Main setID = new Main();
                setID.label1.Text = txtUser.Text;
                setID.Refresh();
                setID.Show();
            }
        }
    }
}
