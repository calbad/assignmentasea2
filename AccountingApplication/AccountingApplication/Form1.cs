﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe; //CB added to allow use of SQL Compact
/*
 *******************************************************************************************
 *                   Accounting Application Built By Callum Baddeley                       *
 * This application allows customer to register, login, edit details and edit bank account *
 *          It also allows admin to manage users, bank accounts and customers              *
 *******************************************************************************************
 */
namespace AccountingApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string _textBox1 //CB added to get data from textBox1(username)
        {
            get { return textBox1.Text; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");//CB added to connect to database
            SqlCeCommand scc = new SqlCeCommand("Select Count(*) From Users where  Username = '" + textBox1.Text + "' and Password = '" + textBox2.Text + "'", mySqlConn);//CB First query to select all users with entered name and return count
            SqlCeCommand sccB = new SqlCeCommand("Select * From Users where  Username = '" + textBox1.Text + "' and Password = '" + textBox2.Text + "'", mySqlConn); //CB Second query to check if user is admin
            mySqlConn.Open(); //CB added to open SQL connection
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())//CB Used to read the data from first query
            {
                using (SqlCeDataReader mySqlDRB = sccB.ExecuteReader())//CB Used to read the data from second query
                {
                    DataTable dt = new DataTable(); //CB Added to create datatable for first query
                    dt.Load(mySqlDR); //CB Added to load first read query into datatable
                    DataTable dtB = new DataTable(); //CB Added to create datatable for first query
                    dtB.Load(mySqlDRB);//CB Added to load first read query into datatable
                    if (dt.Rows[0][0].ToString() == "1" && dtB.Rows[0]["Admin"].ToString() == "1")//CB Checks if user is admin, directs them to admin page
                    {
                        this.Hide();
                        Admin AdLog = new Admin();
                        AdLog.Show();
                    }
                    else if (dt.Rows[0][0].ToString() == "1" && dtB.Rows[0]["Admin"].ToString() != "1")//CB If not admin sent to main page
                    {
                        this.Hide(); //CB Hides Current form
                        Main succLog = new Main(); //CB Creates new instance of main form
                        succLog._textBox = _textBox1; //CB uses above get textBox to pass to Main page to find out which user is logged in
                        succLog.Show(); //CB Shows main form
                    }
                    else
                    {
                        MessageBox.Show("Please Check Your Username and Password");//CB Error thrown if problems with Username or Password
                    }
                }

            }
            
        }

        private void button2_Click(object sender, EventArgs e) //CB Added to allow new users to register
        {
            this.Hide();
            Register reg = new Register();
            reg.Show();
        }
    }
}
