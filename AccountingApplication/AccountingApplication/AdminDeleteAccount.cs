﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    //CB This form is used by admin to delete a bank account
    public partial class AdminDeleteAccount : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        public AdminDeleteAccount()
        {
            InitializeComponent();
            mySqlConn.Open();
        }

        public void numberSearch()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Account where  [Account Number] = '" + txtNumber.Text + "'", mySqlConn);

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    label7.Text = dt.Rows[0]["Account Number"].ToString();
                    label8.Text = dt.Rows[0]["Sort Code"].ToString();
                    Decimal currBal = (decimal)dt.Rows[0]["Balance"];
                    Decimal currBalR = decimal.Round(currBal, 2);
                    label9.Text = currBalR.ToString("0.00");
                    label10.Text = dt.Rows[0]["Cust_ID"].ToString();
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("We Don't Have Any Information Related to This Account Number");
                }
            }
        }

        public void idSearch()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Account where  Cust_ID = '" + txtId.Text + "'", mySqlConn);

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    label7.Text = dt.Rows[0]["Account Number"].ToString();
                    label8.Text = dt.Rows[0]["Sort Code"].ToString();
                    Decimal currBal = (decimal)dt.Rows[0]["Balance"];
                    Decimal currBalR = decimal.Round(currBal, 2);
                    label9.Text = currBalR.ToString("0.00");
                    label10.Text = dt.Rows[0]["Cust_ID"].ToString();
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("We Don't Have Any Information Related to This Customer");
                }
            }
        }

        public void cleartxtBoxes()
        {
            txtNumber.Text = txtId.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            numberSearch();
            cleartxtBoxes();
            this.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            idSearch();
            cleartxtBoxes();
            this.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (label9.Text == "0.00")
            {
                string commandString = "Select * from Account";
                try
                {
                    SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(commandString, new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf"));
                    DataSet dataSet = new DataSet();
                    dataAdapter.Fill(dataSet, "AllAccounts");
                    SqlCeCommandBuilder sqlCb = new SqlCeCommandBuilder(dataAdapter);
                    string delcmd = sqlCb.GetDeleteCommand().CommandText;
                    DataTable t = dataSet.Tables["AllAccounts"];
                    DataRow[] foundRows = t.Select("Cust_ID = '" + label10.Text.Trim() + "'");
                    if (MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        foundRows[0].Delete();
                        dataAdapter.Update(t);
                        label7.Text = " ";
                        label8.Text = " ";
                        label9.Text = " ";
                        MessageBox.Show("Account Deleted");
                        this.Refresh();
                    }
                    else
                    {

                    }

                }
                catch (Exception)
                {
                    MessageBox.Show("No Customer Loaded");
                }
            }
            else
            {
                MessageBox.Show("Account balance must be zero before deletion.");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin admin = new Admin();
            admin.Show();
        }
    }
}
