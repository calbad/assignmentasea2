﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    public partial class AdminEditPersonal : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        String custId;
        String userId;
        public AdminEditPersonal()
        {
            InitializeComponent();
            mySqlConn.Open();
        }

        public void custSearch()
        {
                SqlCeCommand scc = new SqlCeCommand("Select * From Customers where Cust_ID = '" + txtCust.Text + "'", mySqlConn);
                using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    dt.Load(mySqlDR);
                    try
                    {
                        custId = dt.Rows[0]["Cust_ID"].ToString();
                        txtTitle.Text = dt.Rows[0]["Title"].ToString();
                        txtFname.Text = dt.Rows[0]["Firstname"].ToString();
                        txtSname.Text = dt.Rows[0]["Surname"].ToString();
                        txtGender.Text = dt.Rows[0]["Gender"].ToString();
                        txtDob.Text = dt.Rows[0]["DOB"].ToString();
                        txtAdd1.Text = dt.Rows[0]["Address Line 1"].ToString();
                        txtAdd2.Text = dt.Rows[0]["Addresss Line 2"].ToString();
                        txtCounty.Text = dt.Rows[0]["County"].ToString();
                        txtPost.Text = dt.Rows[0]["Postcode"].ToString();
                        txtHome.Text = dt.Rows[0]["Home Number"].ToString();
                        txtMobile.Text = dt.Rows[0]["Mobile Number"].ToString();
                        txtWork.Text = dt.Rows[0]["Work Number"].ToString();
                        txtUserId.Text = dt.Rows[0]["User_ID"].ToString();
                    }
                    catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("We Don't Have Any Information Related to This Customer ID");
                }                
            }
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtTitle.Text) ||
                string.IsNullOrEmpty(txtFname.Text) ||
                string.IsNullOrEmpty(txtSname.Text) ||
                string.IsNullOrEmpty(txtGender.Text) ||
                string.IsNullOrEmpty(txtDob.Text) ||
                string.IsNullOrEmpty(txtAdd1.Text) ||
                string.IsNullOrEmpty(txtAdd2.Text) ||
                string.IsNullOrEmpty(txtCounty.Text) ||
                string.IsNullOrEmpty(txtPost.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public bool checkUser()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Customers where  User_ID = '" + txtUserId.Text + "'", mySqlConn);
            userId = txtUserId.Text;
            bool rtnvalue = true;
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    if (string.IsNullOrEmpty(dt.Rows[0]["User_ID"].ToString()))
                    {
                        rtnvalue = true;
                    }
                    else if (dt.Rows[0]["User_ID"].ToString() == txtUserId.Text && dt.Rows[0]["User_ID"].ToString() != userId)
                    {
                        MessageBox.Show("This User ID is already in use.");
                        rtnvalue = false;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    rtnvalue = true;
                }
                return (rtnvalue);
            }
        }

        public void updateRecord()
        {
            try
            {
                if (string.IsNullOrEmpty(txtTitle.Text))
                {
                    txtUserId.Text = "Null";
                }
                SqlCeCommand cmd = new SqlCeCommand("UPDATE Customers set Title = @title, Firstname = @firstname, Surname = @surname, Gender = @gender, DOB = @dob, [Address Line 1] = @add1, [Addresss Line 2] = @add2, County = @county, Postcode = @postcode, [Home Number] = @home, [Mobile Number] = @mobile, [Work Number] = @work, User_ID = @user WHERE Cust_ID = @cust", mySqlConn);
                cmd.Parameters.AddWithValue("@cust", custId);
                cmd.Parameters.AddWithValue("@title", txtTitle.Text);
                cmd.Parameters.AddWithValue("@firstname", txtFname.Text);
                cmd.Parameters.AddWithValue("@surname", txtSname.Text);
                cmd.Parameters.AddWithValue("@gender", txtGender.Text);
                cmd.Parameters.AddWithValue("@dob", txtDob.Text);
                cmd.Parameters.AddWithValue("@add1", txtAdd1.Text);
                cmd.Parameters.AddWithValue("@add2", txtAdd2.Text);
                cmd.Parameters.AddWithValue("@county", txtCounty.Text);
                cmd.Parameters.AddWithValue("@postcode", txtPost.Text);
                cmd.Parameters.AddWithValue("@home", txtHome.Text);
                cmd.Parameters.AddWithValue("@mobile", txtMobile.Text);
                cmd.Parameters.AddWithValue("@work", txtWork.Text);
                cmd.Parameters.AddWithValue("@user", txtUserId.Text);
                cmd.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void cleartxtBoxes()
        {
            txtCust.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            custSearch();
            cleartxtBoxes();
            this.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (checkInputs() && checkUser())
            {
                updateRecord();
                MessageBox.Show("Details Updated");
                this.Refresh();

            }
        }
    }
}
