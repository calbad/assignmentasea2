﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Forms; //added to gain access to Form elements. Also added a reference to windows.forms 
using AccountingApplication;
using System.Data.SqlServerCe;

namespace AccountTests
{
    [TestClass]
    public class BankTests
    {
        [TestMethod]
        public void TestMethod1()
        {
        }

        [TestMethod]
        public void DebitValid_AccountDebited()
        {
            // arrange
            double openBal = 200;
            double debitAmount = 50.50;
            double expected = 149.50;
            Bank account = new Bank();
            account.setBal = (decimal)openBal;
            account.setDebit = (decimal)debitAmount;

            // act
            account.debitAccount();

            // assert
            decimal dActual = account.newBalance;
            double actual = (double)dActual;
            Assert.AreEqual(expected, actual, 0.001, "Account not debited correctly");
        }

        [TestMethod]
        public void DebitValid_RtnValueTrue()
        {
            // arrange
            double openBal = 200;
            double debitAmount = 50;
            Bank account = new Bank();
            account.setBal = (decimal)openBal;
            account.setDebit = (decimal)debitAmount;

            // act
            account.debitAccount();

            // assert
            bool actual = account.getRtnValue;
            Assert.IsTrue(actual);
        }

        /*[TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void DebitZero_ThrowExp()
        {
            // arrange
            double openBal = 200;
            double debitAmount = -10;
            Bank account = new Bank();
            account.setBal = (decimal)openBal;
            account.setDebit = (decimal)debitAmount;

            // act
            account.debitAccount();

            // assert
            //Assert Handled above public void DebitZero_ThrowExp 
        }*/

        [TestMethod]
        public void DebitZero_RtnValueFalse()
        {
            // arrange
            double openBal = 200;
            double debitAmount = -10;
            Bank account = new Bank();
            account.setBal = (decimal)openBal;
            account.setDebit = (decimal)debitAmount;

            // act
            account.debitAccount();

            // assert
            bool actual = account.getRtnValue;
            Assert.IsFalse(actual);
        }

        /*[TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void DebitAmountMoreThanBalance_ThrowExp()
        {
            // arrange
            double openBal = 200;
            double debitAmount = 210;
            Bank account = new Bank();
            account.setBal = (decimal)openBal;
            account.setDebit = (decimal)debitAmount;

            // act
            account.debitAccount();

            // assert
            //Assert Handled above public void DebitZero_ThrowExp 
        }*/

        [TestMethod]
        public void DebitAmountMoreThanBalance_RtnValueFalse()
        {
            // arrange
            double openBal = 200;
            double debitAmount = 300;
            Bank account = new Bank();
            account.setBal = (decimal)openBal;
            account.setDebit = (decimal)debitAmount;

            // act
            account.debitAccount();

            // assert
            bool actual = account.getRtnValue;
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CreditValid_AccountCredited()
        {
            // arrange
            double openBal = 200;
            double creditAmount = 50.50;
            double expected = 250.50;
            Bank account = new Bank();
            account.setBal = (decimal)openBal;
            account.setCredit = (decimal)creditAmount;

            // act
            account.creditAccount();

            // assert
            decimal dActual = account.newBalance;
            double actual = (double)dActual;
            Assert.AreEqual(expected, actual, 0.001, "Account not debited correctly");
        }

        [TestMethod]
        public void CreditValid_RtnValueTrue()
        {
            // arrange
            double openBal = 200;
            double creditAmount = 50;
            Bank account = new Bank();
            account.setBal = (decimal)openBal;
            account.setDebit = (decimal)creditAmount;

            // act
            account.creditAccount();

            // assert
            bool actual = account.getRtnValue;
            Assert.IsTrue(actual);
        }

        /*[TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CreditZero_ThrowExp()
        {
            // arrange
            double openBal = 200;
            double creditAmount = -10;
            Bank account = new Bank();
            account.setBal = (decimal)openBal;
            account.setDebit = (decimal)creditAmount;

            // act
            account.debitAccount();

            // assert
            //Assert Handled above public void DebitZero_ThrowExp 
        }*/

        [TestMethod]
        public void CreditZero_RtnValueFalse()
        {
            // arrange
            double openBal = 200;
            double creditAmount = -10;
            Bank account = new Bank();
            account.setBal = (decimal)openBal;
            account.setDebit = (decimal)creditAmount;

            // act
            account.creditAccount();

            // assert
            bool actual = account.getRtnValue;
            Assert.IsFalse(actual);
        }        
    }
}
