﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    public partial class AddPersonal : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        public String userId; //CB Created string to set current user ID 
        public AddPersonal()
        {
            InitializeComponent();
            label14.Text = Main._textBox1; //CB Sets label14.Text as username passed from main page using get/set
        }

        public bool checkInputs()//CB checks if all required inputs are filled
        {
            bool rtnvalue = true; //CB bool used to assess whether fields are null or not 

            if (string.IsNullOrEmpty(txtTitle.Text) ||
                string.IsNullOrEmpty(txtFname.Text) ||
                string.IsNullOrEmpty(txtSname.Text) ||
                string.IsNullOrEmpty(txtGender.Text) ||
                string.IsNullOrEmpty(txtDob.Text) ||
                string.IsNullOrEmpty(txtAdd1.Text) ||
                string.IsNullOrEmpty(txtAdd2.Text) ||
                string.IsNullOrEmpty(txtCounty.Text) ||
                string.IsNullOrEmpty(txtPost.Text))
            {
                MessageBox.Show("Error: Please check your inputs"); //CB Message box created to show error message
                rtnvalue = false; //CB sets bool to false to show that one ofthe fields is null.
            }

            return (rtnvalue); //CB returns bool value which will be used later to check whether to insert into database

        }

        public void insertRecord() //CB Method created to insert personal details to database 
        {
            try //CB try method used to test whether data can be inserted to DB
            {
                mySqlConn.Open();
                //CB Sql command created to insert data into Customers table
                SqlCeCommand cmd = new SqlCeCommand("INSERT INTO Customers(Title, Firstname, Surname, Gender, DOB, [Address Line 1], [Addresss Line 2], County, Postcode, [Home Number], [Mobile Number], [Work Number], User_ID) VALUES (@title, @firstname, @surname, @gender, @dob, @add1, @add2, @county, @postcode, @home, @mobile, @work, @user)", mySqlConn);
                //CB Below commands tell Sql command  what values should be assigned to each row
                cmd.Parameters.AddWithValue("@title", txtTitle.Text);
                cmd.Parameters.AddWithValue("@firstname", txtFname.Text);
                cmd.Parameters.AddWithValue("@surname", txtSname.Text);
                cmd.Parameters.AddWithValue("@gender", txtGender.Text);
                cmd.Parameters.AddWithValue("@dob", txtDob.Text);
                cmd.Parameters.AddWithValue("@add1", txtAdd1.Text);
                cmd.Parameters.AddWithValue("@add2", txtAdd2.Text);
                cmd.Parameters.AddWithValue("@county", txtCounty.Text);
                cmd.Parameters.AddWithValue("@postcode", txtPost.Text);
                cmd.Parameters.AddWithValue("@home", txtHome.Text);
                cmd.Parameters.AddWithValue("@mobile", txtMobile.Text);
                cmd.Parameters.AddWithValue("@work", txtWork.Text);
                cmd.Parameters.AddWithValue("@user", userId);
                cmd.ExecuteNonQuery();                
            }
            catch (SqlCeException ex) //CB Catch used in conjunction with try to catch thrown exception and show error message
            {
                MessageBox.Show(label14.Text + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkInputs()) //When Button is clicked the checkInputs method is assessed to see if bool is true,
            {                 //if it is below commands are run.
                insertRecord(); //CB Call to insertRecord Method
                this.Refresh(); //CB refreshes current form
                MessageBox.Show("Details Added");
                this.Close(); //CB closes current form
                Personal personal = new Personal(); 
                personal.Show();
            }
        }
    }
}
