﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    public partial class Main : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        private static Label myLabel;
        String custId;
        public Main()
        {
            InitializeComponent();
            mySqlConn.Open();
            myLabel = label1;
        }

        public string _textBox
        {
            set { myLabel.Text = value; }
        }
        public static string _textBox1
        {
            get { return myLabel.Text; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            MessageBox.Show("You Have Logged Out.");
            Form1 log = new Form1();
            log.Show();
        }

        private void btnPersonal_Click(object sender, EventArgs e)
        {
            Personal personal = new Personal();
            personal.label14.Text = _textBox1;
            SqlCeCommand scc = new SqlCeCommand("Select * From Users where  Username = '" + personal.label14.Text + "'", mySqlConn);
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                SqlCeCommand sccB = new SqlCeCommand("Select * From Customers where User_ID = '" + dt.Rows[0]["ID"].ToString() + "'", mySqlConn);
                String userId = dt.Rows[0]["ID"].ToString();
                using (SqlCeDataReader mySqlDRB = sccB.ExecuteReader())
                {
                    DataTable dtB = new DataTable();
                    dtB.Load(mySqlDRB);
                    try
                    {
                        personal.txtTitle.Text = dtB.Rows[0]["Title"].ToString();
                        personal.txtFname.Text = dtB.Rows[0]["Firstname"].ToString();
                        personal.txtSname.Text = dtB.Rows[0]["Surname"].ToString();
                        personal.txtGender.Text = dtB.Rows[0]["Gender"].ToString();
                        personal.txtDob.Text = dtB.Rows[0]["DOB"].ToString();
                        personal.txtAdd1.Text = dtB.Rows[0]["Address Line 1"].ToString();
                        personal.txtAdd2.Text = dtB.Rows[0]["Addresss Line 2"].ToString();
                        personal.txtCounty.Text = dtB.Rows[0]["County"].ToString();
                        personal.txtPost.Text = dtB.Rows[0]["Postcode"].ToString();
                        personal.txtHome.Text = dtB.Rows[0]["Home Number"].ToString();
                        personal.txtMobile.Text = dtB.Rows[0]["Mobile Number"].ToString();
                        personal.txtWork.Text = dtB.Rows[0]["Work Number"].ToString();
                        custId = dtB.Rows[0]["Cust_ID"].ToString();
                        personal.Show();
                    }
                    catch (IndexOutOfRangeException)
                    {                        
                        AddPersonal add = new AddPersonal();
                        add.userId = userId;
                        add.Show();
                    }
                }
            }            
        }

        private void btnBank_Click(object sender, EventArgs e)
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Users where  Username = '" + label1.Text + "'", mySqlConn);
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                SqlCeCommand sccB = new SqlCeCommand("Select * From Customers where User_ID = '" + dt.Rows[0]["ID"].ToString() + "'", mySqlConn);
                using (SqlCeDataReader mySqlDRB = sccB.ExecuteReader())
                {
                    DataTable dtB = new DataTable();
                    dtB.Load(mySqlDRB);
                    SqlCeCommand sccC = new SqlCeCommand("Select * From Account where Cust_ID = '" + dtB.Rows[0]["Cust_ID"].ToString() + "'", mySqlConn);
                    using (SqlCeDataReader mySqlDRC = sccC.ExecuteReader())
                    {
                        DataTable dtC = new DataTable();
                        dtC.Load(mySqlDRC);
                        try
                        {
                            Bank bank = new Bank();
                            bank.lblAccount.Text = dtC.Rows[0]["Account Number"].ToString();
                            bank.lblSort.Text = dtC.Rows[0]["Sort Code"].ToString();
                            bank.lblType.Text = dtC.Rows[0]["Account Type"].ToString();
                            Decimal currBal = (decimal)dtC.Rows[0]["Balance"];
                            Decimal currBalR = decimal.Round(currBal, 2);
                            bank.lblBal.Text = currBalR.ToString("0.00");                                                        
                            bank.lblAccountId.Text = dtC.Rows[0]["ID"].ToString();
                            bank.lblCustId.Text = dtC.Rows[0]["Cust_ID"].ToString();
                            bank.Show();
                        }
                        catch (IndexOutOfRangeException)
                        {
                            MessageBox.Show("You don't currently have a bank account.");
                        }
                    }
                }
            }
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            this.Hide();
            User user = new User();
            user.txtUser.Text = _textBox1;
            user.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string commandString = "Select * from Users";
            try
            {
                SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(commandString, new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf"));
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "AllUsers");
                SqlCeCommandBuilder sqlCb = new SqlCeCommandBuilder(dataAdapter);
                string delcmd = sqlCb.GetDeleteCommand().CommandText;
                DataTable t = dataSet.Tables["AllUsers"];
                DataRow[] foundRows = t.Select("Username = '" + label1.Text.Trim() + "'");
                if (MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foundRows[0].Delete();
                    dataAdapter.Update(t);
                    MessageBox.Show("User Deleted");
                    this.Close();
                    Form1 log = new Form1();
                    log.Show();

                }
                else
                {
                    
                }                                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(label1 + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
