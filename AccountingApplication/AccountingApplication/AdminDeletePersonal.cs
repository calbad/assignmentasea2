﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    //CB This form is used by admin to delete a customer
    public partial class AdminDeletePersonal : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        public AdminDeletePersonal()
        {
            InitializeComponent();
            mySqlConn.Open();
        }

        public void idSearch()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Customers where  Cust_ID = '" + txtId.Text + "'", mySqlConn);

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    label7.Text = dt.Rows[0]["Cust_ID"].ToString();
                    label8.Text = dt.Rows[0]["Firstname"].ToString();
                    label9.Text = dt.Rows[0]["Surname"].ToString();
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("We Don't Have Any Information Related to This Customer");
                }
            }
        }

        public void cleartxtBoxes()
        {
            txtId.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            idSearch();
            cleartxtBoxes();
            this.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string commandString = "Select * from Customers";
            try
            {
                SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(commandString, new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf"));
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "AllCustomers");
                SqlCeCommandBuilder sqlCb = new SqlCeCommandBuilder(dataAdapter);
                string delcmd = sqlCb.GetDeleteCommand().CommandText;
                DataTable t = dataSet.Tables["AllCustomers"];
                DataRow[] foundRows = t.Select("Cust_ID = '" + label7.Text.Trim() + "'");
                if (MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foundRows[0].Delete();
                    dataAdapter.Update(t);
                    label7.Text = " ";
                    label8.Text = " ";
                    label9.Text = " ";
                    MessageBox.Show("Customer Deleted");
                    this.Refresh();
                }
                else
                {

                }

            }
            catch (Exception)
            {
                MessageBox.Show("No Customer Loaded");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin admin = new Admin();
            admin.Show();
        }
    }
}
