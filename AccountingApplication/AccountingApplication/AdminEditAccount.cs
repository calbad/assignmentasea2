﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    public partial class AdminEditAccount : Form
    {
        //CB This form is used by add min to make changes to bank account, check balance 
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        internal Decimal newBal; //CB set as internal so can be accessed from other forms
        internal Decimal currBal;
        internal Decimal amount;
        public AdminEditAccount()
        {
            InitializeComponent();
            mySqlConn.Open();
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtAccount.Text) ||
                string.IsNullOrEmpty(txtSort.Text) ||
                string.IsNullOrEmpty(txtType.Text))
            {
                MessageBox.Show("Error: Please check your Update Inputs");
                rtnvalue = false;
            }

            return (rtnvalue);
        }

        public void numberSearch()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Account where  [Account Number] = '" + txtNumber.Text + "'", mySqlConn);

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    txtAccount.Text = dt.Rows[0]["Account Number"].ToString();
                    txtSort.Text = dt.Rows[0]["Sort Code"].ToString();
                    txtType.Text = dt.Rows[0]["Account Type"].ToString();
                    Decimal currBal = (decimal)dt.Rows[0]["Balance"];
                    Decimal currBalR = decimal.Round(currBal, 2);
                    lblBalance.Text = currBalR.ToString("0.00");
                    label7.Text = dt.Rows[0]["Cust_ID"].ToString();
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("We Don't Have Any Information Related to This Account Number");
                }
            }
        }

        public void idSearch()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Account where  Cust_ID = '" + txtId.Text + "'", mySqlConn);

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    txtAccount.Text = dt.Rows[0]["Account Number"].ToString();
                    txtSort.Text = dt.Rows[0]["Sort Code"].ToString();
                    txtType.Text = dt.Rows[0]["Account Type"].ToString();
                    Decimal currBal = (decimal)dt.Rows[0]["Balance"];
                    Decimal currBalR = decimal.Round(currBal, 2);
                    lblBalance.Text = currBalR.ToString("0.00");
                    label7.Text = dt.Rows[0]["Cust_ID"].ToString();
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("We Don't Have Any Information Related to This Customer");
                }
            }
        }

        public void cleartxtBoxes()
        {
            txtNumber.Text = txtId.Text = "";
        }        

        public bool creditAccount()
        {
            bool rtnvalue = true;
            if (label7.Text != "label7")
            {
                SqlCeCommand scc = new SqlCeCommand("Select * From Account where  Cust_ID = '" + label7.Text + "'", mySqlConn);
                using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    dt.Load(mySqlDR);
                    try
                    {
                        currBal = (decimal)dt.Rows[0]["Balance"];
                        if (!string.IsNullOrEmpty(txtCredit.Text))
                        {
                            amount = Decimal.Parse(txtCredit.Text.ToString());
                            if (amount <= 0)
                            {
                                MessageBox.Show("Amount To Credit Must Be Greater Than 0.");
                                txtCredit.Text = txtDebit.Text = "";
                                rtnvalue = false;
                            }
                            else
                            {
                                newBal = currBal + amount;
                                rtnvalue = true;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please Enter a Value.");
                            rtnvalue = false;
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        MessageBox.Show("Error! Credit no account loaded");
                        rtnvalue = false;
                    }
                }
            }
            else 
            {
                MessageBox.Show("Error! No account loaded");
                rtnvalue = false;
            }
            return (rtnvalue);
        }

        public bool debitAccount()
        {
            bool rtnvalue = true;
            if (label7.Text != "label7")
            {
                SqlCeCommand scc = new SqlCeCommand("Select * From Account where  Cust_ID = '" + label7.Text + "'", mySqlConn);
                using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    dt.Load(mySqlDR);
                    try
                    {
                        currBal = (decimal)dt.Rows[0]["Balance"];
                        if (!string.IsNullOrEmpty(txtDebit.Text))
                        {
                            amount = Decimal.Parse(txtDebit.Text.ToString());
                            if (amount <= 0)
                            {
                                MessageBox.Show("Amount To Debit Must Be Greater Than 0.");
                                txtCredit.Text = txtDebit.Text = "";
                                rtnvalue = false;
                            }
                            else if (amount > currBal)
                            {
                                MessageBox.Show("You Do Not Have Enough In Your Account To Debit This Amount.");
                                txtCredit.Text = txtDebit.Text = "";
                                rtnvalue = false;
                            }
                            else
                            {
                                newBal = currBal - amount;
                                rtnvalue = true;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please Enter a Value.");
                            rtnvalue = false;
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        MessageBox.Show("Error! Debit no account loaded");
                        rtnvalue = false;
                    }
                }
            }
            else
            {
                MessageBox.Show("Error! No account loaded");
                rtnvalue = false;
            }
            return (rtnvalue);
        }

        public void updateRecord()
        {
            try
            {
                SqlCeCommand cmd = new SqlCeCommand("UPDATE Account SET [Account Number] = @account, [Sort Code] = @sort, [Account Type] = @type WHERE Cust_ID = @cust", mySqlConn);
                cmd.Parameters.AddWithValue("@account", txtAccount.Text);
                cmd.Parameters.AddWithValue("@sort", txtSort.Text);
                cmd.Parameters.AddWithValue("@type", txtType.Text);
                cmd.Parameters.AddWithValue("@cust", label7.Text);
                cmd.ExecuteNonQuery();
            }
            catch (SqlCeException)
            {
                MessageBox.Show("No User Details Loaded.");
            }
        }

        public void updateBalance()
        {
            try
            {
                SqlCeCommand cmd = new SqlCeCommand("UPDATE Account SET Balance = @balance  WHERE Cust_ID = @custId", mySqlConn);
                cmd.Parameters.AddWithValue("@balance", decimal.Round(newBal, 2));
                cmd.Parameters.AddWithValue("@custId", label7.Text);
                cmd.ExecuteNonQuery();
                lblBalance.Text = newBal.ToString("0.00");
                this.Refresh();
            }
            catch (SqlCeException)
            {
                MessageBox.Show("No Account Loaded");
            }
        }

        private void btnNumber_Click(object sender, EventArgs e)
        {
            numberSearch();
            cleartxtBoxes();
            this.Refresh();
        }

        private void btnId_Click(object sender, EventArgs e)
        {
            idSearch();
            cleartxtBoxes();
            this.Refresh();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                updateRecord();
                MessageBox.Show("Account Updated");
                this.Refresh();
            }
        }

        private void btnCredit_Click(object sender, EventArgs e)
        {
            if (creditAccount())
            {
                updateBalance();
                txtCredit.Text = txtDebit.Text = "";
                MessageBox.Show("Account Credited");
            }
        }

        private void btnDebit_Click(object sender, EventArgs e)
        {
            if (debitAccount())
            {
                updateBalance();
                txtCredit.Text = txtDebit.Text = "";
                MessageBox.Show("Account Debited");
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin admin = new Admin();
            admin.Show();
        }
    }
}
