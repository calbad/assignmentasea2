﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccountingApplication
{
    public partial class Admin : Form
    {
        //CB This is the admin page which is used as the central hub for admin user to choose an option
        public Admin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            MessageBox.Show("You Have Logged Out.");
            Form1 log = new Form1();
            log.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminAddUser user = new AdminAddUser();
            user.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminEditUser user = new AdminEditUser();
            user.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminDeleteUser delete = new AdminDeleteUser();
            delete.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminAddAccount addBank = new AdminAddAccount();
            addBank.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminEditAccount editBank = new AdminEditAccount();
            editBank.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminDeleteAccount deleteBank = new AdminDeleteAccount();
            deleteBank.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminAddPersonal addPersonal = new AdminAddPersonal();
            addPersonal.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminEditPersonal editPersonal = new AdminEditPersonal();
            editPersonal.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminDeletePersonal deletePersonal = new AdminDeletePersonal();
            deletePersonal.Show();
        }        
    }
}
