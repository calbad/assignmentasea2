﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    //CB This form is used by admin to delete a user from the database
    public partial class AdminDeleteUser : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        String adminChecked = "0"; //CB string created to set a value that can be use later in conjunction wit check box
        public AdminDeleteUser()
        {
            InitializeComponent();
            mySqlConn.Open();
        }

        public void userSearch()//CB search facility added so admin can search for user by username
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Users where  Username = '" + txtUser.Text + "'", mySqlConn);

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    label7.Text = dt.Rows[0]["Username"].ToString();
                    label8.Text = dt.Rows[0]["Email"].ToString();
                    adminChecked = dt.Rows[0]["Admin"].ToString();

                    if (adminChecked == "1") //CB Sets the state of the check box depending on value from database
                    {
                        checkBox1.Checked = true;
                    }
                    else if (adminChecked == "0")
                    {
                        checkBox1.Checked = false;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("We Don't Have Any Information Related to This User");
                }
            }
        }

        public void idSearch() //CB search facility added so admin can search for user by ID
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Users where  ID = '" + txtId.Text + "'", mySqlConn);

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    label7.Text = dt.Rows[0]["Username"].ToString();
                    label8.Text = dt.Rows[0]["Email"].ToString();
                    adminChecked = dt.Rows[0]["Admin"].ToString();

                    if (adminChecked == "1")
                    {
                        checkBox1.Checked = true;
                    }
                    else if (adminChecked == "0")
                    {
                        checkBox1.Checked = false;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("We Don't Have Any Information Related to This User");
                }
            }
        }

        public void cleartxtBoxes()
        {
            txtUser.Text = txtId.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            userSearch();
            cleartxtBoxes();
            this.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            idSearch();
            cleartxtBoxes();
            this.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string commandString = "Select * from Users"; //CB sets the query as a string
            try
            {
                //CB Data adapter used to set command and connection for the dataset
                SqlCeDataAdapter dataAdapter = new SqlCeDataAdapter(commandString, new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf"));
                //CB Dataset used to store the information loaded from command
                DataSet dataSet = new DataSet();
                //CB Fills dataAdapter with data set and table data 
                dataAdapter.Fill(dataSet, "AllUsers");
                //CB Builds Sql command from above information
                SqlCeCommandBuilder sqlCb = new SqlCeCommandBuilder(dataAdapter);
                //CB New string that is used to set the delete command
                string delcmd = sqlCb.GetDeleteCommand().CommandText;
                //CB Loads dataset into a data table
                DataTable t = dataSet.Tables["AllUsers"];
                //CB creats an array of rows based on the select query
                DataRow[] foundRows = t.Select("Username = '" + label7.Text.Trim() + "'");
                //CB Used to confirm that admin wants to proceed with action as cannot be undone
                if (MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foundRows[0].Delete(); //CB The found row is deleted 
                    dataAdapter.Update(t); //CB Table is update
                    label7.Text = " "; //CB labels that store user info are cleared
                    label8.Text = " ";
                    checkBox1.Checked = false; //CB sets check box state as unchecked
                    MessageBox.Show("User Deleted"); //CB confirms user deleted
                    this.Refresh(); //CB refreshes form
                }
                else
                {

                }

            }
            catch (Exception)
            {
                MessageBox.Show("No Customer Loaded"); //CB error thrown if admin tries to delete without searching for customer
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin admin = new Admin();
            admin.Show();
        }
    }
}
