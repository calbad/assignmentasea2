﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    //CB This form is used by admin to add users to the database
    public partial class AdminAddUser : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        public AdminAddUser()
        {
            InitializeComponent();
            mySqlConn.Open();
        }

        public void cleartxtBoxes()
        {
            txtUser.Text = txtPass.Text = txtConf.Text = txtEmail.Text = "";
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtUser.Text) ||
                string.IsNullOrEmpty(txtPass.Text) ||
                string.IsNullOrEmpty(txtConf.Text) ||
                string.IsNullOrEmpty(txtEmail.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public bool checkPassword()
        {
            bool rtnvalue = true;

            if (txtPass.Text != txtConf.Text)
            {
                MessageBox.Show("Error: Passwords do not match. Please Try Again");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public bool checkUser()//CB Checks if username is already in use
        {            
            SqlCeCommand scc = new SqlCeCommand("Select Count(*) From Users where  Username = '" + txtUser.Text + "'", mySqlConn);

            bool rtnvalue = true;
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {

                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                if (dt.Rows[0][0].ToString() == "1")
                {
                    MessageBox.Show("This Username is already taken, please choose another");
                    rtnvalue = false;
                }

                return (rtnvalue);
            }
        }

        public bool checkEmail()//CB Checks if email is already in use
        {           
            SqlCeCommand scc = new SqlCeCommand("Select Count(*) From Users where  Email = '" + txtEmail.Text + "'", mySqlConn);

            bool rtnvalue = true;
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {

                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                if (dt.Rows[0][0].ToString() == "1")
                {
                    MessageBox.Show("This Email is already in use, please choose another");
                    rtnvalue = false;
                }

                return (rtnvalue);
            }
        }


        public void insertRecord(String user, String pass, String email, String admin, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConn);
                cmdInsert.Parameters.AddWithValue("@user", user);
                cmdInsert.Parameters.AddWithValue("@pass", pass);
                cmdInsert.Parameters.AddWithValue("@email", email);
                cmdInsert.Parameters.AddWithValue("@admin", admin);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(user + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin admin = new Admin();
            admin.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {            
            if (checkInputs() && checkPassword() && checkUser() && checkEmail())
            {

                String commandString = "INSERT INTO Users(Username, Password, Email, Admin) VALUES (@user, @pass, @email, @admin)";
                if (checkBox1.Checked)
                {
                    checkBox1.Text = "1";
                }
                else if (!checkBox1.Checked)
                {
                    checkBox1.Text = "0";
                }
                insertRecord(txtUser.Text, txtPass.Text, txtEmail.Text, checkBox1.Text, commandString);
                cleartxtBoxes();
                this.Hide();
                MessageBox.Show("User Created.");
                Admin admin = new Admin();
                admin.Show();
            }
        }
    }
}
