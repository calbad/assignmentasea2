﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    //CB This form is used for admin to add a customer
    public partial class AdminAddPersonal : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        public AdminAddPersonal()
        {
            InitializeComponent();
            mySqlConn.Open();
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtTitle.Text) ||
                string.IsNullOrEmpty(txtFname.Text) ||
                string.IsNullOrEmpty(txtSname.Text) ||
                string.IsNullOrEmpty(txtGender.Text) ||
                string.IsNullOrEmpty(txtDob.Text) ||
                string.IsNullOrEmpty(txtAdd1.Text) ||
                string.IsNullOrEmpty(txtAdd2.Text) ||
                string.IsNullOrEmpty(txtCounty.Text) ||
                string.IsNullOrEmpty(txtPost.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public bool checkUser()
        {
            SqlCeCommand scc = new SqlCeCommand("Select * From Customers where  User_ID = '" + txtUserId.Text + "'", mySqlConn);

            bool rtnvalue = true;
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                try
                {
                    if (string.IsNullOrEmpty(dt.Rows[0]["User_ID"].ToString()))
                    {
                        rtnvalue = true;
                    }
                    else if (dt.Rows[0]["User_ID"].ToString() == txtUserId.Text) //CB Checks to see if the change User ID is already in use
                    {
                        MessageBox.Show("This User already has details");
                        rtnvalue = false;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    rtnvalue = true;
                }
                    return (rtnvalue);
            }
        }

        public void insertRecord()
        {
            try
            {
                if (string.IsNullOrEmpty(txtTitle.Text))
                {
                    txtUserId.Text = "Null";
                }
                SqlCeCommand cmd = new SqlCeCommand("INSERT INTO Customers(Title, Firstname, Surname, Gender, DOB, [Address Line 1], [Addresss Line 2], County, Postcode, [Home Number], [Mobile Number], [Work Number], User_ID) VALUES (@title, @firstname, @surname, @gender, @dob, @add1, @add2, @county, @postcode, @home, @mobile, @work, @user)", mySqlConn);
                cmd.Parameters.AddWithValue("@title", txtTitle.Text);
                cmd.Parameters.AddWithValue("@firstname", txtFname.Text);
                cmd.Parameters.AddWithValue("@surname", txtSname.Text);
                cmd.Parameters.AddWithValue("@gender", txtGender.Text);
                cmd.Parameters.AddWithValue("@dob", txtDob.Text);
                cmd.Parameters.AddWithValue("@add1", txtAdd1.Text);
                cmd.Parameters.AddWithValue("@add2", txtAdd2.Text);
                cmd.Parameters.AddWithValue("@county", txtCounty.Text);
                cmd.Parameters.AddWithValue("@postcode", txtPost.Text);
                cmd.Parameters.AddWithValue("@home", txtHome.Text);
                cmd.Parameters.AddWithValue("@mobile", txtMobile.Text);
                cmd.Parameters.AddWithValue("@work", txtWork.Text);
                cmd.Parameters.AddWithValue("@user", txtUserId.Text);
                cmd.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(label14.Text + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkInputs() && checkUser())
            {
                insertRecord();
                this.Refresh();
                MessageBox.Show("Details Added");
            }
        }
    }
}
