﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace AccountingApplication
{
    public partial class Personal : Form
    {
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEA\assignmentasea2\AccountingApplication\MyDatabase1.sdf");
        public Personal()
        {
            InitializeComponent();
            label14.Text = Main._textBox1;
            extractDetails();
        }
        String userId;
        String custId;
        public void extractDetails()
        {            
            SqlCeCommand scc = new SqlCeCommand("Select * From Users where  Username = '" + label14.Text + "'", mySqlConn);
            mySqlConn.Open();

            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                SqlCeCommand sccB = new SqlCeCommand("Select * From Customers where User_ID = '" + dt.Rows[0]["ID"].ToString() + "'", mySqlConn);
                userId = dt.Rows[0]["ID"].ToString();                
                using (SqlCeDataReader mySqlDRB = sccB.ExecuteReader())
                {                                        
                    DataTable dtB = new DataTable();
                    dtB.Load(mySqlDRB);
                    try
                    {
                        txtTitle.Text = dtB.Rows[0]["Title"].ToString();
                        txtFname.Text = dtB.Rows[0]["Firstname"].ToString();
                        txtSname.Text = dtB.Rows[0]["Surname"].ToString();
                        txtGender.Text = dtB.Rows[0]["Gender"].ToString();
                        txtDob.Text = dtB.Rows[0]["DOB"].ToString();
                        txtAdd1.Text = dtB.Rows[0]["Address Line 1"].ToString();
                        txtAdd2.Text = dtB.Rows[0]["Addresss Line 2"].ToString();
                        txtCounty.Text = dtB.Rows[0]["County"].ToString();
                        txtPost.Text = dtB.Rows[0]["Postcode"].ToString();
                        txtHome.Text = dtB.Rows[0]["Home Number"].ToString();
                        txtMobile.Text = dtB.Rows[0]["Mobile Number"].ToString();
                        txtWork.Text = dtB.Rows[0]["Work Number"].ToString();
                        custId = dtB.Rows[0]["Cust_ID"].ToString();                  
                    }
                    catch (IndexOutOfRangeException)
                    {
                        MessageBox.Show("We Don't Have Any Information Stored for You. Please Enter Your Details in the Following Form");              
                    }
                }
            }
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtTitle.Text) ||
                string.IsNullOrEmpty(txtFname.Text) ||
                string.IsNullOrEmpty(txtSname.Text) ||
                string.IsNullOrEmpty(txtGender.Text) ||
                string.IsNullOrEmpty(txtDob.Text) ||
                string.IsNullOrEmpty(txtAdd1.Text) ||
                string.IsNullOrEmpty(txtAdd2.Text) ||
                string.IsNullOrEmpty(txtCounty.Text) ||
                string.IsNullOrEmpty(txtPost.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public void updateRecord()
        {
            try
            {
                SqlCeCommand cmd = new SqlCeCommand("UPDATE Customers set Title = @title, Firstname = @firstname, Surname = @surname, Gender = @gender, DOB = @dob, [Address Line 1] = @add1, [Addresss Line 2] = @add2, County = @county, Postcode = @postcode, [Home Number] = @home, [Mobile Number] = @mobile, [Work Number] = @work, User_ID = @user WHERE Cust_ID = @cust", mySqlConn);
                cmd.Parameters.AddWithValue("@cust", custId);
                cmd.Parameters.AddWithValue("@title", txtTitle.Text);
                cmd.Parameters.AddWithValue("@firstname", txtFname.Text);
                cmd.Parameters.AddWithValue("@surname", txtSname.Text);
                cmd.Parameters.AddWithValue("@gender", txtGender.Text);
                cmd.Parameters.AddWithValue("@dob", txtDob.Text);
                cmd.Parameters.AddWithValue("@add1", txtAdd1.Text);
                cmd.Parameters.AddWithValue("@add2", txtAdd2.Text);
                cmd.Parameters.AddWithValue("@county", txtCounty.Text);
                cmd.Parameters.AddWithValue("@postcode", txtPost.Text);
                cmd.Parameters.AddWithValue("@home", txtHome.Text);
                cmd.Parameters.AddWithValue("@mobile", txtMobile.Text);
                cmd.Parameters.AddWithValue("@work", txtWork.Text);
                cmd.Parameters.AddWithValue("@user", userId);
                cmd.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(custId + userId + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                updateRecord();
                MessageBox.Show("Details Updated");
                this.Refresh();
                
            }
        }
    }
}
